from operator import index
import numpy as np
from sklearn.datasets import load_digits
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
import matplotlib
import matplotlib.pyplot as plt
import seaborn as sns
import pandas as pd
import time
import umap

exams = pd.read_csv('lesson6/exams.csv')
print(exams.head())

exams = exams.dropna()
print(exams.parental_level_of_education)

print(exams.parental_level_of_education.value_counts())

sns.set(style='darkgrid', context='paper', rc={'figure.figsize':(14,10)})

g = sns.PairGrid(exams, hue="parental_level_of_education")
g.map_diag(sns.histplot)
g.map_diag(sns.histplot, multiple="stack", element="step")
g.map_offdiag(sns.scatterplot)
g.add_legend()
g.savefig('lesson6/exams.png')
plt.close()


reducer = umap.UMAP()
exams_data = exams[
    [
        "math_score",
        "reading_score",
        "writing_score"
    ]
].values

scaled_exams_data = StandardScaler().fit_transform(exams_data)

embedding = reducer.fit_transform(scaled_exams_data)
print(embedding.shape)

plt.figure()
plt.scatter(
    embedding[:, 0],
    embedding[:, 1],
    c=[sns.color_palette()[x] for x in exams.parental_level_of_education.map({"some college":0, "associate's degree":1, "high school":2, "some high school":3, "bachelor's degree":4, "master's degree":5})])
plt.gca().set_aspect('equal', 'datalim')
plt.title('UMAP projection of the exams dataset', fontsize=24)
plt.savefig('lesson6/exams_embeddings.png')