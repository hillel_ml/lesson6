from operator import index
import numpy as np
from sklearn.datasets import load_digits
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
import matplotlib
import matplotlib.pyplot as plt
import seaborn as sns
import pandas as pd
import time
import umap
import umap.plot
import hdbscan

forestfires = pd.read_csv('lesson6/forestfires.csv')
print(forestfires.head())

forestfires = forestfires.dropna()
print(forestfires.month)

print(forestfires.month.value_counts())

sns.set(style='darkgrid', context='paper', rc={'figure.figsize':(14,10)})

g = sns.PairGrid(forestfires, hue="month")
g.map_diag(sns.histplot)
g.map_diag(sns.histplot, multiple="stack", element="step")
g.map_offdiag(sns.scatterplot)
g.add_legend()
g.savefig('lesson6/forestfires.png')
plt.close()


reducer = umap.UMAP(
    n_neighbors=30,
    min_dist=0.0,
    n_components=2,
    random_state=42
)


forestfires_data = forestfires[
    [
        "FFMC",
        "DMC",
        "DC",
        "ISI",
        "temp",
        "RH",
        "wind",
        "rain",
        "area"
    ]
].values

scaled_forestfires_data = StandardScaler().fit_transform(forestfires_data)

embedding = reducer.fit_transform(scaled_forestfires_data)
print(embedding.shape)

plt.scatter(
    embedding[:, 0],
    embedding[:, 1],
    c=[sns.color_palette()[x] for x in forestfires.month.map({
        "aug":0,
        "sep":1,
        "mar":2,
        "jul":3,
        "feb":4,
        "jun":5,
        "oct":6,
        "apr":7,
        "dec":8,
        "jan":9
        })
        ]
    )
plt.show()

# umap.plot.points(embedding, forestfires.month)

# plt.figure()
# plt.scatter(
#     embedding[:, 0],
#     embedding[:, 1],
#     c=[sns.color_palette()[x] for x in forestfires.month.map({
        # "aug":0,
        # "sep":1,
        # "mar":2,
        # "jul":3,
        # "feb":4,
        # "jun":5,
        # "oct":6,
        # "apr":7,
        # "dec":8,
        # "jan":9
        # })
        # ]
#     )
# plt.gca().set_aspect('equal', 'datalim')
# plt.title('UMAP projection of the forestfires dataset', fontsize=24)
# plt.savefig('lesson6/forestfires_embeddings.png')



# hdbscan_labels = hdbscan.HDBSCAN(
#     min_samples=10,
#     min_cluster_size=500
#     ).fit_predict(embedding)






# hdbscan_labels = hdbscan_labels.labels_

# print(hdbscan_labels)

# embeddings_df = pd.DataFrame()

# embeddings_df["Cluster"] = hdbscan_labels

# print(embeddings_df)

# clustered = (hdbscan_labels >= 0)

# plt.figure()
# plt.scatter(embedding[:, 0],
#             embedding[:, 1],
#             c=hdbscan_labels,
#             )
# plt.gca().set_aspect('equal', 'datalim')
# plt.title('HDBSCAN projection of the forestfires dataset', fontsize=24)
# plt.savefig('lesson6/forestfires_hdbscan.png')







# clustered = (labels >= 0)
# plt.scatter(standard_embedding[~clustered, 0],
#             standard_embedding[~clustered, 1],
#             color=(0.5, 0.5, 0.5),
#             s=0.1,
#             alpha=0.5)
# plt.scatter(standard_embedding[clustered, 0],
#             standard_embedding[clustered, 1],
#             c=labels[clustered],
#             s=0.1,
#             cmap='Spectral')




# embedding['Clusters']=clusterer.labels_


# modified_data = hdbscan.HDBSCAN(
#     min_samples=10,
#     min_cluster_size=500,
# ).fit_predict(embedding)



# c=[sns.color_palette()[x] for x in forestfires.month.map({"aug":0, "sep":1, "mar":2, "jul":3, "feb":4, "jun":5, "oct":6, "apr":7, "dec":8, "jan":9, "may":10, "nov":11})])
# c=[sns.color_palette()[x] for x in forestfires.day.map({"sun":0, "fri":1, "sat":2, "mon":3, "tue":4, "thu":5, "wed":6})])