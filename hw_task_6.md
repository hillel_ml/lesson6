1. Взять любой датасет для задачи классификации. Если не получается найти, то использовать Fashion MNIST
2. Понизить размерность с помощью алгоритма UMAP
3. Показать график с датасетом в пространстве двух измерений.
4. Использовать алгоритм hdbscan для поиска классов.
5. Показать полученные классы
6. (опционально) Предложить способ оценки качества полученного решения
7. (опционально) Сравнить полученные результаты с применением других подходов для данного датасета. Например, с помощью персептрона мы получали на MNIST точность около 97%, а сейчас с использованием UMAP+hdbscan - ??%. Можно поискать готовые решения на соревнованиях Kaggle.





https://www.kaggle.com/code/ahmetarslann/stroke-prediction

Encoding
le = LabelEncoder()
df["gender"] = le.fit_transform(df["gender"])
df["ever_married"] = le.fit_transform(df["ever_married"])
df["Residence_type"] = le.fit_transform(df["Residence_type"])





https://www.kaggle.com/code/subhranildey85/stroke-prediction-with-6-models-and-5-matrics

https://www.kaggle.com/code/akashkotal/stroke-disease-eda-with-7-machine-learning-model

https://www.kaggle.com/code/anubhavgoyal10/stroke-prediction-100-accuracy