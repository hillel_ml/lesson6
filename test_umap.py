
from operator import index
import numpy as np
from sklearn.datasets import load_digits
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
import matplotlib
import matplotlib.pyplot as plt
import seaborn as sns
import pandas as pd
import time
import umap

### Example 1

# penguins = pd.read_csv("https://github.com/allisonhorst/palmerpenguins/raw/5b5891f01b52ae26ad8cb9755ec93672f49328a8/data/penguins_size.csv")
# penguins.to_csv('penguins.csv', index=False)

penguins = pd.read_csv('lesson6/penguins.csv',)
print(penguins.head())

penguins = penguins.dropna()
print(penguins.species_short)

print(penguins.species_short.value_counts())

sns.set(style='darkgrid', context='paper', rc={'figure.figsize':(14,10)})

g = sns.PairGrid(penguins, hue="species_short")
g.map_diag(sns.histplot)
g.map_diag(sns.histplot, multiple="stack", element="step")
g.map_offdiag(sns.scatterplot)
g.add_legend()
g.savefig('lesson6/penguins.png')
plt.close()


reducer = umap.UMAP()
penguin_data = penguins[
    [
        "culmen_length_mm",
        "culmen_depth_mm",
        "flipper_length_mm",
        "body_mass_g",
    ]
].values
scaled_penguin_data = StandardScaler().fit_transform(penguin_data)

embedding = reducer.fit_transform(scaled_penguin_data)
print(embedding.shape)

plt.figure()
plt.scatter(
    embedding[:, 0],
    embedding[:, 1],
    c=[sns.color_palette()[x] for x in penguins.species_short.map({"Adelie":0, "Chinstrap":1, "Gentoo":2})])
plt.gca().set_aspect('equal', 'datalim')
plt.title('UMAP projection of the Penguin dataset', fontsize=24)
plt.savefig('lesson6/embeddings.png')

# #### Example 2

# digits = load_digits()
# #print(digits.DESCR)

# plt.figure()
# fig, ax_array = plt.subplots(20, 20)
# axes = ax_array.flatten()
# for i, ax in enumerate(axes):
#     ax.imshow(digits.images[i], cmap='gray_r')
# plt.setp(axes, xticks=[], yticks=[], frame_on=False)
# plt.tight_layout(h_pad=0.5, w_pad=0.01)
# plt.savefig('digits.png')
# plt.close()

# plt.figure()
# digits_df = pd.DataFrame(digits.data[:,1:11])
# digits_df['digit'] = pd.Series(digits.target).map(lambda x: 'Digit {}'.format(x))
# g = sns.PairGrid(digits_df, hue="digit")
# #g.map_diag(sns.histplot)
# g.map_diag(sns.histplot, multiple="stack", element="step")
# g.map_offdiag(sns.scatterplot)
# g.add_legend()
# g.savefig('digits_pairs.png')
# plt.close()

# reducer = umap.UMAP(random_state=42)
# reducer.fit(digits.data)

# # UMAP(a=None,
# #      angular_rp_forest=False,
# #      b=None,
# #      force_approximation_algorithm=False,
# #      init='spectral',
# #      learning_rate=1.0,
# #      local_connectivity=1.0,
# #      low_memory=False,
# #      metric='euclidean',
# #      metric_kwds=None,
# #      min_dist=0.1,
# #      n_components=2,
# #      n_epochs=None,
# #      n_neighbors=15,
# #      negative_sample_rate=5,
# #      output_metric='euclidean',
# #      output_metric_kwds=None,
# #      random_state=42,
# #      repulsion_strength=1.0,
# #      set_op_mix_ratio=1.0,
# #      spread=1.0,
# #      target_metric='categorical',
# #      target_metric_kwds=None,
# #      target_n_neighbors=-1,
# #      target_weight=0.5,
# #      transform_queue_size=4.0,
# #      transform_seed=42,
# #      unique=False,
# #      verbose=False)

# embedding = reducer.transform(digits.data)
# print(embedding.shape)

# plt.figure(figsize=(10, 10))
# ax = plt.gca()
# plt.scatter(embedding[:, 0], embedding[:, 1], c=digits.target, cmap='Spectral', s=36, linewidths=0.5, edgecolors='black')
# #plt.gca().set_aspect('equal', 'datalim')
# ax.set_xticks(np.arange(-10, 20, 2))
# ax.set_yticks(np.arange(-10, 20, 2))
# plt.colorbar(boundaries=np.arange(11)-0.5).set_ticks(np.arange(10))
# plt.title('UMAP projection of the Digits dataset', fontsize=24)
# plt.grid(color='black', linestyle='--', linewidth=0.5)
# plt.savefig('digits_embedding.png')













