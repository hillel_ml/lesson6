from operator import index
import numpy as np
from sklearn.datasets import load_digits
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
import matplotlib
import matplotlib.pyplot as plt
import seaborn as sns
import pandas as pd
import time
import umap

adm_ds = pd.read_csv('lesson6/adm_data.csv')
print(adm_ds.head())

adm_ds = adm_ds.dropna()
print(adm_ds.chance)

print(adm_ds.chance.value_counts())

sns.set(style='darkgrid', context='paper', rc={'figure.figsize':(14,10)})

g = sns.PairGrid(adm_ds, hue="chance")
g.map_diag(sns.histplot)
g.map_diag(sns.histplot, multiple="stack", element="step")
g.map_offdiag(sns.scatterplot)
g.add_legend()
g.savefig('lesson6/adm_data.png')
plt.close()


reducer = umap.UMAP()
adm_ds_data = adm_ds[
    [
        "Serial_num",
        "GRE_score",
        "TOEFL_score",
        "University_Rating",
        "SOP",
        "LOR",
        "CGPA",
        "Research"
    ]
].values

scaled_adm_ds_data = StandardScaler().fit_transform(adm_ds_data)

embedding = reducer.fit_transform(scaled_adm_ds_data)
print(embedding.shape)

plt.figure()
plt.scatter(
    embedding[:, 0],
    embedding[:, 1],
    c=[sns.color_palette()[x] for x in adm_ds.chance.map({"0.45":0, "0.60":1, "0.75":2, "0.90":3})])
plt.gca().set_aspect('equal', 'datalim')
plt.title('UMAP projection of the adm_data dataset', fontsize=24)
plt.savefig('lesson6/adm_data_embeddings.png')




#      c=[sns.color_palette()[x] for x in adm_ds.chance.map({"some college":0, "associate's degree":1, "high school":2, "some high school":3, "bachelor's degree":4, "master's degree":5})])
