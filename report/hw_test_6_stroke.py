from operator import index
import numpy as np
from sklearn.datasets import load_digits
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
import matplotlib
import matplotlib.pyplot as plt
import seaborn as sns
import pandas as pd
import time
import umap
import hdbscan

stroke_ds = pd.read_csv('lesson6/stroke_data.csv')
print(stroke_ds.head())

# Removing NaN values
stroke_ds = stroke_ds.dropna()

print(stroke_ds.stroke)

print(stroke_ds.stroke.value_counts())


# Removing the "id" column
stroke_ds = stroke_ds.drop(columns=["id"])


# Assigning Binary Values to Two-Value Qualitative Characteristics
gender_map = {"Female":0, "Male":1}
stroke_ds["gender"] = stroke_ds['gender'].map(gender_map)


married_map = {"Yes":0, "No":1}
stroke_ds["ever_married"] = stroke_ds['ever_married'].map(married_map)


married_map = {"Rural":0, "Urban":1}
stroke_ds["residence_type"] = stroke_ds['residence_type'].map(married_map)

# One hot encoding - Assigning a table of binary values for qualitative characteristics with multiple values
one_hot_encoded_data = pd.get_dummies(stroke_ds, columns = ['work_type', 'smoking_status'])

# Move the "stroke" column to the end of the table
one_hot_encoded_data.insert(17, 'stroke', one_hot_encoded_data.pop('stroke'))

# Renaming some columns
one_hot_encoded_data = one_hot_encoded_data.rename(columns={"work_type_Self-employed": "work_type_Self", "smoking_status_formerly smoked": "smoking_status_formerly", "smoking_status_never smoked": "smoking_status_never"})


# We build a matrix of scatterplots with paired functions in order to get an idea of the data structure
sns.set(style='darkgrid', context='paper', rc={'figure.figsize':(14,10)})

g = sns.PairGrid(one_hot_encoded_data, hue="stroke")
g.map_diag(sns.histplot)
g.map_diag(sns.histplot, multiple="stack", element="step")
g.map_offdiag(sns.scatterplot)
g.add_legend()
g.savefig('lesson6/stroke.png')
plt.close()


# Constructing a UMAP object and setting parametres
reducer = umap.UMAP(
    n_neighbors=15, # default value is 15
    min_dist=0.0, # default value is 0.1
    # n_components=2, # default value is 2
    metric='mahalanobis'
)


# Converting each feature into z-scores (number of standard deviations from the mean) for comparability
stroke_data = one_hot_encoded_data[
    [
        "gender",
        "age",
        "hypertension",
        "heart_disease",
        "ever_married",
        "residence_type",
        "avg_glucose_level",
        "bmi",
        "work_type_Govt_job",
        "work_type_Never_worked",
        "work_type_Private",
        "work_type_Self",
        "work_type_children",
        "smoking_status_Unknown",
        "smoking_status_formerly",
        "smoking_status_never",
        "smoking_status_smokes"
    ]
].values

scaled_stroke_data = StandardScaler().fit_transform(stroke_data)


# For reducing representation of the data we will use, the fit_transform method
# which first calls fit and then returns the transformed data as a numpy array
embedding = reducer.fit_transform(scaled_stroke_data)
print(embedding.shape)


# Visualization of results
plt.figure()
plt.scatter(
    embedding[:, 0],
    embedding[:, 1],
    c=[sns.color_palette()[x] for x in one_hot_encoded_data.stroke])
plt.gca().set_aspect('equal', 'datalim')
plt.title('UMAP projection of the stroke dataset', fontsize=24)
plt.savefig('lesson6/stroke_embeddings.png')


# Appling hdbscan and set parameters
hdbscan_labels = hdbscan.HDBSCAN(
    min_samples=10,
    min_cluster_size=500
).fit_predict(embedding)


# Visualization of results
plt.figure()
plt.scatter(embedding[:, 0],
            embedding[:, 1],
            c=hdbscan_labels,
            )
plt.gca().set_aspect('equal', 'datalim')
plt.title('HDBSCAN projection of the stroke dataset', fontsize=24)
plt.savefig('lesson6/stroke_hdbscan.png')





# ====== ЗАПЧАСТИ ======

# hdbscan_labels = hdbscan_labels.labels_

# print(hdbscan_labels)

# embeddings_df = pd.DataFrame()

# embeddings_df["Cluster"] = hdbscan_labels

# print(embeddings_df)

# clustered = (hdbscan_labels >= 0)





# clustered = (labels >= 0)
# plt.scatter(standard_embedding[~clustered, 0],
#             standard_embedding[~clustered, 1],
#             color=(0.5, 0.5, 0.5),
#             s=0.1,
#             alpha=0.5)
# plt.scatter(standard_embedding[clustered, 0],
#             standard_embedding[clustered, 1],
#             c=labels[clustered],
#             s=0.1,
#             cmap='Spectral')




# embedding['Clusters']=clusterer.labels_


# modified_data = hdbscan.HDBSCAN(
#     min_samples=10,
#     min_cluster_size=500,
# ).fit_predict(embedding)



# c=[sns.color_palette()[x] for x in forestfires.month.map({"aug":0, "sep":1, "mar":2, "jul":3, "feb":4, "jun":5, "oct":6, "apr":7, "dec":8, "jan":9, "may":10, "nov":11})])
# c=[sns.color_palette()[x] for x in forestfires.day.map({"sun":0, "fri":1, "sat":2, "mon":3, "tue":4, "thu":5, "wed":6})])