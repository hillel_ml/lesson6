import numpy as np
import pandas as pd




df = pd.read_csv('lesson6/df_test/stroke_data.csv')
print(df.head())


df = df.drop(columns=["id"])
print(df.head())


gender_map = {"Female":0, "Male":1}
df["gender"] = df['gender'].map(gender_map)
print(df.head())

married_map = {"Yes":0, "No":1}
df["ever_married"] = df['ever_married'].map(married_map)
print(df.head())

married_map = {"Rural":0, "Urban":1}
df["residence_type"] = df['residence_type'].map(married_map)
print(df.head())



one_hot_encoded_data = pd.get_dummies(df, columns = ['work_type', 'smoking_status'])
print(one_hot_encoded_data)



one_hot_encoded_data.insert(17, 'stroke', one_hot_encoded_data.pop('stroke'))


cols1 = one_hot_encoded_data.columns.tolist()
print(cols1)



print(type(one_hot_encoded_data))



# cols2 = cols1[:8]
# cols3 = cols1[8:]
# cols4 = cols1[8]

# cols1 = cols2 + cols3 + cols4
# print(cols2)
# print(cols3)
# print(cols1)

